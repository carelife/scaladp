package adapter.java;

public class TextShape implements IShape {
	private String text;
	
	public TextShape(String text) {
		this.text = text;
	}
	
	@Override
	public void draw() {
		new TextDrawer().paint(text);
		
	}

}
