package adapter.java;

public interface IShape {
	public void draw();
}
