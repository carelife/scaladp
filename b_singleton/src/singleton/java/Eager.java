package singleton.java;

public class Eager {
	
	private static Eager instance = new Eager();
	
	public static Eager getInstance() {
		return instance;
	}

}
